package CarApp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CarWriter {
    public static void main(String[] args) throws Exception {
        String uin;
        // String color;
        String mark, model;
        int year;
        double price;
        Scanner sc = new Scanner(System.in);
        Car car1 = new Car("", "", "", "", 0, 0.0);
        {
            System.out.println("Введите уин,марку,модель,год,цену");
            uin = sc.nextLine();
            mark = sc.nextLine();
            model = sc.nextLine();
            year = sc.nextInt();
            price = sc.nextDouble();
            //пошли присваивать данные объекту
            car1.setUin(uin);
            car1.setMark(mark);
            car1.setModel(model);
            car1.setYear(year);
            car1.setPrice(price);
            sc.close();
            System.out.println("Вот это мы вот тут создали: " + car1);
            String separator = File.separator;//чтобы слеш в пути перевернулся для IOS или windows
            String path = separator + "D:" + separator + "TeamIDEA" + separator + "src" + separator + "CarApp" + separator + "cars.dat";
            File file = new File(path);
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));//тру для добавки текста
                bw.write(car1.toString());
                bw.flush();//освобождаем буфер, до его заполнения
                bw.write("\n");
                bw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }


        }


    }
}
